ifndef VERBOSE
MAKEFLAGS += --no-print-directory
endif

.PHONY: help
help: ## Lists the available commands.
# Add a comment with '##' to describe a command.
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

.PHONY: clean
clean: ## Clean all cmake files
	rm -rf build

.PHONY: update
update: ## Update cmake files
	mkdir -p build
	cmake -B build

.PHONY: regenerate
regenerate: clean update ## Regenerate cmake files

.PHONY: build
build: ## Build the program
	cd build && make -j4

.PHONY: run
run: ## Run the program
	build/Gottlicht

.PHONY: exec
exec: build run ## Build and then run the program

.PHONY: all
all: regenerate exec ## Start from scratch and run the program
