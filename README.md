# gottlicht

An attempt to turn my lights on and off by whistling. Manual override and push button activation are also supported.

This code works in concert with the following CAD design: [onshape](https://cad.onshape.com/documents/d7d0d7e22d37c2a706aaf0c4/w/85510c8f35e8c0a87c59a7b8/e/74768f15f78b3426a597955e). Additionally, a raspberry PI and various hardware components are needed (motor, microphone, ADC) that is configured perfectly so that the GPIO pins line up.

Overall, it was a partial success ... everything worked, up until the motor didn't have enough torque to fully press the light switch, slightly depressing it before bouncing off. Either a better motor or a gearbox are needed to solve this problem.

![setup](results/setup.png)
![mount](results/mount.png)
