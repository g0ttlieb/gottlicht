#pragma once

#include <G0TTLIEB/spi.hpp>
#include <thread>

class AudioCapture{
public:
    AudioCapture(std::string spi_device, int block_size);
    ~AudioCapture();

    static int get_freq();

    bool has_data();
    std::vector<double> get_data();

private:
    G0TTLIEB::Spi mic_adc;
    int block_size;
    int current_buffer;
    int current_index;
    bool buffer_ready;
    std::vector<double> buffers[2];

    bool finished;
    std::thread capture_loop;
    void capture_audio();
    void add_sample(double val);
};
