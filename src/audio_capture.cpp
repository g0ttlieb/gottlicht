#include "audio_capture.hpp"

const int FREQ = 24000;
const int CLOCK_PER_SAMPLE = 64;
const int CLOCK_HZ = FREQ * CLOCK_PER_SAMPLE;

AudioCapture::AudioCapture(std::string spi_device, int block_size):
        mic_adc(spi_device, G0TTLIEB::Spi::Mode::ZERO, CLOCK_HZ),
        block_size(block_size),
        current_buffer(0),
        current_index(0),
        buffer_ready(false),
        finished(false){
    buffers[0].resize(block_size);
    buffers[1].resize(block_size);
    capture_loop = std::thread(&AudioCapture::capture_audio, this);
}

AudioCapture::~AudioCapture(){
    finished = true;
    capture_loop.join();
}

int AudioCapture::get_freq(){
    return FREQ;
}

bool AudioCapture::has_data(){
    return buffer_ready;
}

std::vector<double> AudioCapture::get_data(){
    if(!buffer_ready)
        return std::vector<double>();
    buffer_ready = false;
    return buffers[!current_buffer];
}

void AudioCapture::capture_audio(){
    while(!finished){
        unsigned char data[3] = {1, 0b10100000, 0};
        mic_adc.transfer(data, data, 3);
        short raw = ((data[1] & 0b00001111) << 8) | data[2];
        double val = (((double) raw / 4096) * 2.f) - 1.f;
        add_sample(val);
    }
}

void AudioCapture::add_sample(double val){
    buffers[current_buffer][current_index++] = val;
    if(current_index == block_size){
        current_index = 0;
        current_buffer = !current_buffer;
        buffer_ready = true;
    }
}
