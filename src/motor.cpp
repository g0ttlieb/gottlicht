#include "motor.hpp"
#include <unistd.h>

const int DRIVE_TIME = 1000000; // 1 second

Motor::Motor(std::shared_ptr<G0TTLIEB::GpioChip> chip):
    current_position(true),
    motorDir1(chip, 5, "motor_dir1", false),
    motorDir2(chip, 6, "motor_dir2", false),
    motorEnable(chip, 13, "motor_enable", 500, 1.0, false){
}

void Motor::toggle_position(){
    motorDir1.write_state(current_position);
    motorDir2.write_state(!current_position);
    current_position = !current_position;

    motorEnable.start();
    usleep(DRIVE_TIME);
    motorEnable.stop();
}
