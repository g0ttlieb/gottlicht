#pragma once

#include <G0TTLIEB/gpio.hpp>

class Motor{
public:
    Motor(std::shared_ptr<G0TTLIEB::GpioChip> chip);
    void toggle_position();

private:
    bool current_position;
    G0TTLIEB::GpioPinOut motorDir1;
    G0TTLIEB::GpioPinOut motorDir2;
    G0TTLIEB::GpioPinPwm motorEnable;
};
