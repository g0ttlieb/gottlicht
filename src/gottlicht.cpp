#include "trigger.hpp"
#include "motor.hpp"

int main(){
    auto chip = G0TTLIEB::GpioChip::make_gpio_chip("gpiochip0");
    Trigger trigger(chip);
    Motor motor(chip);

    while(true){
        trigger.wait_till_triggered();
        motor.toggle_position();
    }
}
