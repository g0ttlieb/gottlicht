#include "trigger.hpp"
#include <unistd.h>

const int SLEEP_TIME = 10000; // 0.01 seconds
const int DEBOUNCE_TIME = 500000; // 0.5 seconds

const int BLOCK_SIZE = 8192;
const int LOWER_FREQ = 700;
const int UPPER_FREQ = 2100;

const int NARROWBAND_BANDWIDTH = 25;
const int BROADBAND_BANDWIDTH = 150;
const int NARROWBAND_POWER_THRESHOLD = 15;
const int BROADBAND_POWER_RATIO = 3;

const double FREQ = AudioCapture::get_freq();
const double FREQ_INCREMENT = FREQ / BLOCK_SIZE;
const int START_INDEX = (LOWER_FREQ - BROADBAND_BANDWIDTH) / FREQ_INCREMENT;
const int END_INDEX = (UPPER_FREQ + BROADBAND_BANDWIDTH) / FREQ_INCREMENT;
const int NARROWBAND_SIZE = NARROWBAND_BANDWIDTH / FREQ_INCREMENT;
const int BROADBAND_SIZE = BROADBAND_BANDWIDTH / FREQ_INCREMENT;

Trigger::Trigger(std::shared_ptr<G0TTLIEB::GpioChip> chip):
    led(chip, 20, "gottlicht_led", false),
    button(chip, 21, "gottlicht_button"),
    fft(BLOCK_SIZE),
    audio_capture("/dev/spidev0.0", BLOCK_SIZE),
    currently_triggered(false){
}

void Trigger::wait_till_triggered(){
    while(true){
        usleep(SLEEP_TIME);
        if(triggered_state_unchanged())
            continue;
        led.write_state(currently_triggered);
        if(currently_triggered)
            return;
        usleep(DEBOUNCE_TIME);
    }
}

bool Trigger::triggered_state_unchanged(){
    bool was_triggered = currently_triggered;
    currently_triggered = (button.read_state()) ||
                          (audio_capture.has_data() && audio_contains_trigger());
    return (was_triggered == currently_triggered);
}

bool Trigger::audio_contains_trigger(){
    std::vector<double> audio_block = audio_capture.get_data();
    std::vector<std::complex<double>> freq_domain = fft.execute(audio_block);

    std::vector<double> powers(END_INDEX - START_INDEX);
    for(int i = START_INDEX; i < END_INDEX; i++)
        powers[i - START_INDEX] = std::abs(freq_domain[i]);

    int max_index = 0;
    double max_power = -1;
    for(unsigned int i = BROADBAND_SIZE; i < powers.size() - BROADBAND_SIZE; i++){
        if(powers[i] > max_power){
            max_power = powers[i];
            max_index = i;
        }
    }

    double narrowband_power = 0.0;
    for(int i = -NARROWBAND_SIZE; i < NARROWBAND_SIZE; i++)
        narrowband_power += powers[i + max_index];
    narrowband_power /= 2.0 * (double) NARROWBAND_SIZE;

    double broadband_power = 0.0;
    for(int i = -BROADBAND_SIZE; i < BROADBAND_SIZE; i++)
        broadband_power += powers[i + max_index];
    broadband_power /= 2.0 * (double) BROADBAND_SIZE;

    return (narrowband_power > NARROWBAND_POWER_THRESHOLD) &&
           (narrowband_power > BROADBAND_POWER_RATIO * broadband_power);
}
