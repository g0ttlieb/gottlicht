#pragma once

#include <G0TTLIEB/fft.hpp>
#include <G0TTLIEB/gpio.hpp>
#include "audio_capture.hpp"

class Trigger {
public:
    Trigger(std::shared_ptr<G0TTLIEB::GpioChip> chip);
    void wait_till_triggered();

private:
    G0TTLIEB::GpioPinOut led;
    G0TTLIEB::GpioPinIn button;
    G0TTLIEB::RealFft fft;
    AudioCapture audio_capture;

    bool currently_triggered;

    bool triggered_state_unchanged();
    bool audio_contains_trigger();
};
